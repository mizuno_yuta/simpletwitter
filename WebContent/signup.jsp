<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored = "false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">

		<c:if test="${not empty errorMessages }">
			<div class="errorMessagesr">
				<ul>
					<c:forEach items = "${errorMessages }" var="errorMessages">
						<li><c:out value="${errorMessages }" />
					</c:forEach>
				</ul>
			</div>
		</c:if>

		<form action="signup" method="post"><br />
			<label for="name">名前</label>
			<input name="name" id="name" />(名前はあなたの公開プロフィールに表示されます)<br />

			<label for="account">アカウント名</label>
			<input name="account" id="account"> <br />

			<label for="password">パスワード</label>
			<input name="password" type="password" id="password"> <br />

			<label for="email">メールアドレス</label>
			<input name="email" id="email"> <br />

			<label for="discription">説明</label>
			<textarea name="discription" cols="35" rows="35" id="discription"></textarea> <br />

			<input type="submit" value="登録" /> <br />
			<a href="./">戻る</a>
		</form>

		<div class="copyright">Copyright(c)Mizuno</div>
	</div>

	<c:if test="${ not empty errorMessages }">
    <div class="errorMessages">
        <ul>
            <c:forEach items="${errorMessages}" var="errorMessage">
                <li><c:out value="${errorMessage}" />
            </c:forEach>
        </ul>
    </div>
    <c:remove var="errorMessages" scope="session" />
</c:if>

<div class="form-area">
    <c:if test="${ isShowMessageForm }">
        <form action="message" method="post">
            いま、どうしてる？<br />
            <textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
            <br />
            <input type="submit" value="つぶやく">（140文字まで）
        </form>
    </c:if>
</div>
            <div class="copyright"> Copyright(c)YourName</div>
</body>
</html>