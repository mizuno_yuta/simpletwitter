package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId, String start, String end) {

		final int LIMIT_NUM = 1000;
		Connection connection = null;
		try {
			connection = getConnection();
			Integer id = null;
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}
			//start入力日付
			if(!StringUtils.isEmpty(start)) {
				start = start + " 00:00:00";
			} else {
				start = "2020-01-01 00:00:00";
			}

			//end入力日付
			if(!StringUtils.isEmpty(end)) {
				end = end + " 23:59:59";
			} else {
				Date date = new Date();
				end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, start, end);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//編集画面にtextとだす
	public Message editSelect(String messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			int id = Integer.parseInt(messageId);
			Message message = new MessageDao().editSelect(connection, id);
			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	//変更したtextをdaoに送る
	public void edit(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().edit(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}